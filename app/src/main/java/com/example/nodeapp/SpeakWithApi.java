package com.example.nodeapp;

import android.content.Intent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SpeakWithApi {
    private String _ApiUrl = "http://jeremialangenwalder.ddns.net:8393/";
    private boolean IsLogginIn = false;


    public void GetJwtToken(UserModel _User, MainActivity mainActivity) {
        if(!IsLogginIn) {
            IsLogginIn = true;
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(_ApiUrl + "api/DoAccess/" + _User.Username + "/" + _User.Password)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                    IsLogginIn = false;
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        String myResponse = response.body().string();

                        JSONObject main = null;
                        UserModel _Result = new UserModel();
                        try {
                            main = new JSONObject(myResponse);
                            _Result.Username = main.getString("userName");
                            _Result.Password = main.getString("userPassword");
                            _Result.UserID = main.getInt("userID");
                            _Result.JwtToken = main.getString("jwtToken");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                        mainActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mainActivity.DoLogin(_Result);
                            }
                        });


                        IsLogginIn = false;
                    }
                }
            });
        }
    }

    public void GetNotesForOverview(UserModel user, OverviewNotes overview) {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(_ApiUrl + "api/Notes/0/" + user.UserID)
                .addHeader("Authorization", "Bearer " + user.JwtToken)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    ArrayList<NotesModel> result = new ArrayList<>();
                    try {
                        String Json = response.body().string();
                        JSONArray array = new JSONArray(Json);
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject nodeJson = array.getJSONObject(i);
                            result.add(new NotesModel(
                                    nodeJson.getInt("noteID"),
                                    nodeJson.getString("header"),
                                    nodeJson.getString("noteText")
                            ));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    overview.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            overview.NotesToView(result);
                        }
                    });
                }
            }
        });
    }

    public void PostNote(UserModel user, NotesModel note, SingleViewNotes view){
        JSONObject json = new JSONObject();
        try{
            json.put("NoteID", 0);
            json.put("UserID", user.UserID);
            json.put("NoteText", note.GetTitle());
            json.put("Header", note.GetText());
        }
        catch (JSONException e){
            e.printStackTrace();
        }

        OkHttpClient client = new OkHttpClient();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, json.toString());
        Request request = new Request.Builder()
                .url(_ApiUrl + "api/Notes")
                .addHeader("Authorization", "Bearer " + user.JwtToken)
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()){
                    String Json = response.body().string();
                    view.AddOrEditFinsihed();
                }
            }
        });
    }

    public void PutNote(UserModel user, NotesModel note, SingleViewNotes view) {
        JSONObject json = new JSONObject();
        try{
            json.put("NoteID", note.GetID());
            json.put("UserID", user.UserID);
            json.put("NoteText", note.GetTitle());
            json.put("Header", note.GetText());
        }
        catch (JSONException e){
            e.printStackTrace();
        }

        OkHttpClient client = new OkHttpClient();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, json.toString());
        Request request = new Request.Builder()
                .url(_ApiUrl + "api/Notes/" + note.GetID())
                .addHeader("Authorization", "Bearer " + user.JwtToken)
                .put(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()){
                    String Json = response.body().string();
                    view.AddOrEditFinsihed();
                }
            }
        });
    }

    public void DeleteNote(UserModel user, NotesModel note, SingleViewNotes view) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(_ApiUrl + "api/Notes/" + note.GetID())
                .addHeader("Authorization", "Bearer " + user.JwtToken)
                .delete()
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()){
                    String Json = response.body().string();
                    view.AddOrEditFinsihed();
                }
            }
        });
    }
}

