package com.example.nodeapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class MainActivity extends AppCompatActivity {

    private Button btnLogin;
    private EditText txtPassword;
    private EditText txtUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StartUp();
    }

    private void StartUp(){
        btnLogin = findViewById( R.id.btnLogin);
        txtPassword = findViewById(R.id.txtPassword);
        txtUsername = findViewById(R.id.txtUsername);
        btnLogin.setOnClickListener((View v) -> { btnLogin_Click(v);});
    }

    public void btnLogin_Click(View v) {
        UserModel _User = new UserModel();
        _User.Password = txtPassword.getText().toString();
        _User.Username = txtUsername.getText().toString();

        SpeakWithApi api = new SpeakWithApi();
        api.GetJwtToken(_User, this);


        Snackbar.make(v, "Logging in . . .", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    public void DoLogin(UserModel user){
        if(user.Username == "ERROR" || user.Password == "ERROR" || user.JwtToken == "" || user.UserID == 0){
            AlertDialog alert = new AlertDialog.Builder(this)
                    .setTitle("Fehler!")
                    .setMessage("Username oder Password sind falsch!")
                    .show();
            return;
        }
        Intent Newpage = new Intent(this, OverviewNotes.class);
        Newpage.putExtra("UserModel", user);
        startActivity(Newpage);
    }


}
