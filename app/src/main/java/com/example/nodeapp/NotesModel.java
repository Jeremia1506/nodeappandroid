package com.example.nodeapp;

import java.io.Serializable;

public class NotesModel implements Serializable {

    private int _ID = 0;
    private String _Title = "";
    private String _NoteText = "";

    public NotesModel(int ID, String Title, String NoteText){
        _ID = ID;
        _Title = Title;
        _NoteText = NoteText;
    }

    public void SetID(int id){_ID = id;}
    public int GetID(){return _ID;}

    public void SetTitle(String title){_Title = title;}
    public String GetTitle(){return _Title;}

    public void SetText(String text){_NoteText = text;}
    public String GetText(){return _NoteText;}
}
