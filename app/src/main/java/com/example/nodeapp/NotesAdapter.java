package com.example.nodeapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class NotesAdapter extends ArrayAdapter<NotesModel> {

    private static final String Tag = "PersonListAdapter";

    private Context _Context;

    private int _Resource;

    public NotesAdapter (Context context, int ressource, List<NotesModel> objects){
        super(context, ressource, objects);
        _Context = context;
        _Resource = ressource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String Title = getItem(position).GetTitle();
        String Text = getItem(position).GetText();
        int ID = getItem(position).GetID();

        NotesModel note = new NotesModel(ID, Title, Text);

        LayoutInflater inflater = LayoutInflater.from(_Context);
        convertView = inflater.inflate(_Resource, parent, false);

        TextView tvHeader= convertView.findViewById(R.id.Title);
        TextView tvText= convertView.findViewById(R.id.Text);
        TextView tvID = convertView.findViewById( R.id.NoteID);

        tvHeader.setText(Title);
        tvText.setText(Text);
        tvID.setText(Integer.toString(ID));

        return convertView;
    }
}
