package com.example.nodeapp;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SingleViewNotes extends AppCompatActivity {

    private TextView tvHeader;
    private TextView tvText;
    private Button btnAddOrEdit;
    private Button btnDelete;

    private UserModel _User;
    private NotesModel _Note;
    private SpeakWithApi _Api;


    private boolean AddNote = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_view_notes);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        _Api = new SpeakWithApi();

        _User = (UserModel) getIntent().getSerializableExtra("UserModel");
        tvHeader = findViewById(R.id.NoteHeader);
        tvText = findViewById(R.id.NoteText);
        btnAddOrEdit = findViewById((R.id.AddOrEdit));
        btnAddOrEdit.setOnClickListener((View v) -> btnAddOrEdit_Click(v));
        btnDelete = findViewById(R.id.Delete);
        btnDelete.setOnClickListener((View v) -> btnDelete_Click(v));

        NotesModel note = (NotesModel) getIntent().getSerializableExtra("NotesModel");
        if(note != null){
            _Note = note;
            AddNote = false;
            tvHeader.setText(note.GetTitle());
            tvText.setText(note.GetText());
        }
    }

    public void btnAddOrEdit_Click(View v) {
        Snackbar.make(v, "Füge Notiz hinzu . . .", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
        NotesModel note = new NotesModel(
                0,
                tvText.getText().toString(),
                tvHeader.getText().toString());
        if (AddNote) {
            _Api.PostNote(_User, note, this);
            return;
        }
        note.SetID(_Note.GetID());
        _Api.PutNote(_User, note, this);
    }

    public void btnDelete_Click(View v){
        if(AddNote){
            Snackbar.make(v, "Dafür muss eine Notiz erst erstellt werden! . . .", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return;
        }
        Snackbar.make(v, "Notiz wird gelöscht! . . .", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
        _Api.DeleteNote(_User, new NotesModel(_Note.GetID(), "" , ""), this);
    }

    public void AddOrEditFinsihed(){
            Intent Newpage = new Intent(this, OverviewNotes.class);
            Newpage.putExtra("UserModel", _User);
            startActivity(Newpage);
    }
}
