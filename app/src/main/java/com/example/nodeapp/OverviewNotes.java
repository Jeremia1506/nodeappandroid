package com.example.nodeapp;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class OverviewNotes extends AppCompatActivity {

    private ListView lvNotes;
    private Button testButton;
    private UserModel _User;
    private SpeakWithApi _Api = new SpeakWithApi();
    private ArrayList<NotesModel> _UserNotes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overview_notes);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);


        fab.setOnClickListener((View v) -> NewNot_Click(v));

        _User = (UserModel) getIntent().getSerializableExtra("UserModel");

        lvNotes = findViewById(R.id.lvNotes);

        lvNotes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent Newpage = new Intent(view.getContext() , SingleViewNotes.class);
                Newpage.putExtra("UserModel", _User);
                Newpage.putExtra("NotesModel", _UserNotes.get(i));
                startActivity(Newpage);
            }
        });

        _Api.GetNotesForOverview(_User, this);
    }

    private void NewNot_Click(View v){
            Intent Newpage = new Intent(v.getContext(), SingleViewNotes.class);
            Newpage.putExtra("UserModel", _User);
            startActivity(Newpage);
    }

    public void NotesToView(ArrayList<NotesModel> notes){
        _UserNotes = notes;
        NotesAdapter adapter = new NotesAdapter(this, R.layout.row, notes);
        lvNotes.setAdapter(adapter);
    }
}
